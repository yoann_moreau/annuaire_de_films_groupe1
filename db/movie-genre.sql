#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: movies
#------------------------------------------------------------

CREATE TABLE movies(
        id_movie     Int  Auto_increment  NOT NULL ,
        title        Varchar (255) NOT NULL ,
        description  Text NOT NULL ,
        release_date Date NOT NULL ,
        director     Varchar (255) NOT NULL
	,CONSTRAINT movies_PK PRIMARY KEY (id_movie)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: genres
#------------------------------------------------------------

CREATE TABLE genres(
        id_genre Int  Auto_increment  NOT NULL ,
        name     Varchar (70) NOT NULL
	,CONSTRAINT genres_PK PRIMARY KEY (id_genre)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: have
#------------------------------------------------------------

CREATE TABLE have(
        id_genre Int NOT NULL ,
        id_movie Int NOT NULL
	,CONSTRAINT have_PK PRIMARY KEY (id_genre,id_movie)

	,CONSTRAINT have_genres_FK FOREIGN KEY (id_genre) REFERENCES genres(id_genre)
	,CONSTRAINT have_movies0_FK FOREIGN KEY (id_movie) REFERENCES movies(id_movie)
)ENGINE=InnoDB;

