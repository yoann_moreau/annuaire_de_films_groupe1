// Variables
var currentImage = 0;

// functions
function positionMovies() {
    var movieList = document.getElementById('movie-list');
    var movieInfos = document.getElementsByClassName('movie-info');

    movieList.classList.add('positioned');
    for (var i = 0; i < movieInfos.length; i++) {
        movieInfos[i].classList.add('positioned');
    }
}

function delayPosition() {
    setTimeout(positionMovies, 100);
}

function carouselActivation() {
    var carousel = document.getElementById('carousel');
    var images = carousel.getElementsByTagName('img');

    images[0].classList.add('active');

    setInterval(carouselRotation, 4000);
}

function carouselRotation() {
    var carousel = document.getElementById('carousel');
    var images = carousel.getElementsByTagName('img');

    if (currentImage < images.length - 1) {
        currentImage++;
    } else {
        currentImage = 0;
    }

    for (let i = 0; i < images.length; i++) {
        images[i].classList.remove('active');
    }
    images[currentImage].classList.add('active');
}

// Event listener
document.body.onload = delayPosition;

// Code to execute
carouselActivation();
