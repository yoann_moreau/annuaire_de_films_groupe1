<?php
require '../model/singleMovie.php';

require_once '../vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('../view');
$twig = new Twig_Environment($loader);

$id = $_GET['id'];

// Twig variables
echo $twig->render("movie.twig",
    ["movie" => movie($id),
    "genre" => genre($id),
    "poster" => poster($id),
    "actor" => actor($id)
]
);
