<?php
require("model/movies.php");

require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('view');
$twig = new Twig_Environment($loader);

if (isset($_POST['research'])) {
    $search = search();
    $orderFunction = [];

} else {
    $search = [];
    $orderFunction = title();

    if (isset($_GET['order'])) {
        $order = $_GET['order'];
        if ($order === 'DESC') {
            $orderFunction = DESC_title();
        } else {
            $orderFunction = title();
        }
    }
}

// Twig variables
echo $twig->render("index.twig",
    ["thumbnails" => thumbnails(),
    "title" => $orderFunction,
    "search" => $search,
    ]
);
