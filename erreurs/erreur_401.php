<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Error 401</title>
    <link rel='stylesheet/less' href='../resources/less/style.less'>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <script src='../resources/js/less.js' type='text/javascript'></script>
</head>

<body>
    <header id='main-header'>
        <div id='title'>
            <img src='../resources/img/logo.png'>
            <h1>Modaba</h1>
        </div>
    </header>
    <section id='carousel'>
    </section>
    <section id='movies'>
        <ul>
            <li class='movie'>
              <br>
               <h2>Error 401 : Unhautorized</h2>
               <p>l'utilisateur n'a pas entré le bon mot de passe pour accéder au contenu.</p>
                <div class='film'>
                </div>
            </li>
        </ul>
    </section>

    <script type='text/javascript' src='../resources/js/document.js'></script>
</body>

</html>