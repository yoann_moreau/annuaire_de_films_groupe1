-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 26 oct. 2018 à 11:13
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bdd_annuaire`
--

-- --------------------------------------------------------

--
-- Structure de la table `movies`
--

DROP TABLE IF EXISTS `movies`;
CREATE TABLE IF NOT EXISTS `movies` (
  `id_movie` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `release_date` date NOT NULL,
  `director` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_movie`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `movies`
--

INSERT INTO `movies` (`id_movie`, `title`, `description`, `release_date`, `director`, `thumbnail`) VALUES
(1, 'Reservoir Dogs', 'Après un hold-up manqué des cambrioleurs de haut vol font leurs comptes dans une confrontation violente pour découvrir lequel d\'entre eux les a trahis.', '1992-09-02', 'Quentin Tarantino', 'reservoir_dogs.jpg'),
(2, 'Spider-Man', 'Après avoir été mordu par une araignée génétiquement modifiée qui s\'était échappée d\'un laboratoire il se découvre des pouvoirs surhumain', '2002-06-12', 'Sam Raimi', 'spiderman.jpg'),
(3, 'Scott Pilgrim', 'Scott Pilgrim a 22 ans vit à Toronto et joue dans le groupe de rock amateur Mais Scott ne pourra conquérir le cœur de Ramona qu\'après avoir vaincu ses sept ex maléfiques', '2010-12-01', 'Edgar Wright', 'scott_pilgrim.jpg'),
(4, 'Alice aux pays des merveilles', 'Alice désormais âgée de 19 ans retourne dans le monde fantastique qu\'elle a découvert quand elle était enfant. Alice s\'embarque alors dans une aventure extraordinaire où elle accomplira son destin.', '2010-03-24', 'Tim Burton', 'alice_in_wonderlands.jpg'),
(5, 'Coco', 'Depuis déjà plusieurs générations la musique est bannie dans la famille de Miguel. Un vrai déchirement pour le jeune garçon dont le rêve ultime est de devenir un musicien aussi accompli que son idole: Ernesto de la Cruz', '2017-11-29', 'Lee Unrich Adrian Molina', 'coco.jpg'),
(6, 'Les mondes de Ralph', 'Dans une salle d’arcade Ralph la casse est le héros mal aimé d’un jeu des années 80. Son rôle est simple : il casse tout ! Pourtant il ne rêve que d’une chose être aimé de tous…', '2012-12-05', 'Rich Moore', 'les_mondes_de_ralph.jpg'),
(7, 'La cité de la peur', 'Une comédie familiale', '1994-03-09', 'Alain Berberian', 'lcdlp_photo1.jpg'),
(8, 'Interstellar', 'La recherche d\'une destination pour la migration de l\'espèce humaine', '2014-11-05', 'Christopher Nolan', 'interstellar.jpg'),
(9, 'Fight Club', 'La rencontre entre une vie banale et l\'anti-conformisme', '1999-11-10', 'David Fincher', 'fight_club.jpg'),
(10, 'Warcraft, le commencent', 'Adaptation du premier opus des jeux de la série Warcraft, les Orcs envahissent le monde d\'Azeroth', '2016-05-24', 'Duncan Jones', 'warcraft.jpg'),
(11, 'Ready player One', 'Le monde de 2045 est nul,  tous le monde préfère vivre dans son monde virtuel', '2018-03-28', 'Steven Spielberg', 'ready_player_one.jpg'),
(12, 'Avengers: Infinity war', 'Thanos menace l\'équilibre de l\'univers tout entier, les héros doivent lutter pour empêcher la purge inter-planétaire', '2018-04-23', 'Anthony et Joe Russo', 'infinity_war.jpg');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
