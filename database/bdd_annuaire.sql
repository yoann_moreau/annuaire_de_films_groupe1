-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 26 oct. 2018 à 13:02
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bdd_annuaire`
--

-- --------------------------------------------------------

--
-- Structure de la table `actor`
--

DROP TABLE IF EXISTS `actor`;
CREATE TABLE IF NOT EXISTS `actor` (
  `id_actor` int(11) NOT NULL AUTO_INCREMENT,
  `actor` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_actor`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `actor`
--

INSERT INTO `actor` (`id_actor`, `actor`) VALUES
(1, 'Harvey Keitel'),
(2, 'Tim Roth'),
(3, 'Tobey Maguire'),
(4, 'Kirsten Dunst'),
(5, 'Michael Cera'),
(6, 'Mary Elizabeth Winstead'),
(7, 'Mia Wasikowska'),
(8, 'Johnny Depp'),
(9, 'Gael García Bernal : Héctor (voix)'),
(10, 'Benjamin Bratt	(voix)'),
(11, 'Sarah Silverman'),
(12, 'Jack McBrayer'),
(13, 'Alain Chabat'),
(14, 'Dominique Farrugia'),
(15, 'Matthew McConaughey'),
(16, 'Anne Hathaway'),
(17, 'Edward Norton'),
(18, 'Brad Pitt'),
(19, 'Travis Fimmel'),
(20, 'Paula Patton'),
(21, 'Tye Sheridan'),
(22, 'Olivia Cooke'),
(23, 'Scarlett Johansson'),
(24, 'Chris Pratt');

-- --------------------------------------------------------

--
-- Structure de la table `genres`
--

DROP TABLE IF EXISTS `genres`;
CREATE TABLE IF NOT EXISTS `genres` (
  `id_genre` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_genre`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `genres`
--

INSERT INTO `genres` (`id_genre`, `name`) VALUES
(1, 'Policier'),
(2, 'Thriller'),
(3, 'Fantastique'),
(4, 'Action'),
(5, 'Aventure'),
(6, 'Comédie'),
(7, 'Animation'),
(8, 'Science-Fiction'),
(9, 'Drame'),
(10, 'Satire Sociale');

-- --------------------------------------------------------

--
-- Structure de la table `have`
--

DROP TABLE IF EXISTS `have`;
CREATE TABLE IF NOT EXISTS `have` (
  `id_genre` int(11) NOT NULL,
  `id_movie` int(11) NOT NULL,
  PRIMARY KEY (`id_genre`,`id_movie`),
  KEY `have_movies0_FK` (`id_movie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `have`
--

INSERT INTO `have` (`id_genre`, `id_movie`) VALUES
(1, 1),
(2, 1),
(3, 2),
(4, 2),
(3, 3),
(5, 3),
(6, 3),
(3, 4),
(7, 5),
(7, 6),
(6, 7),
(8, 8),
(2, 9),
(9, 9),
(10, 9),
(3, 10),
(8, 11),
(3, 12),
(4, 12);

-- --------------------------------------------------------

--
-- Structure de la table `have_actor`
--

DROP TABLE IF EXISTS `have_actor`;
CREATE TABLE IF NOT EXISTS `have_actor` (
  `id_movie` int(11) NOT NULL,
  `id_actor` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_actor`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `have_actor`
--

INSERT INTO `have_actor` (`id_movie`, `id_actor`) VALUES
(1, 1),
(1, 2),
(2, 3),
(2, 4),
(3, 5),
(3, 6),
(4, 7),
(4, 8),
(5, 9),
(5, 10),
(6, 11),
(6, 12),
(7, 13),
(7, 14),
(8, 15),
(8, 16),
(9, 17),
(9, 18),
(10, 19),
(10, 20),
(11, 21),
(11, 22),
(12, 23),
(12, 24);

-- --------------------------------------------------------

--
-- Structure de la table `have_img`
--

DROP TABLE IF EXISTS `have_img`;
CREATE TABLE IF NOT EXISTS `have_img` (
  `id_movie` int(11) NOT NULL,
  `id_img` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `have_img`
--

INSERT INTO `have_img` (`id_movie`, `id_img`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(11, 11),
(12, 12);

-- --------------------------------------------------------

--
-- Structure de la table `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `id_img` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(200) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id_img`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `images`
--

INSERT INTO `images` (`id_img`, `image`, `text`) VALUES
(1, 'reservoir_dogs_poster.jpg', ''),
(2, 'spiderman_poster.jpg', ''),
(3, 'scott_pilgrim_poster.jpg', ''),
(4, 'alice_in_wonderlands_poster.jpg', ''),
(5, 'coco_poster.jpg', ''),
(6, 'les_mondes_de_ralph_poster.jpg', ''),
(7, 'la_cité_de_la_peur_poster.jpg', ''),
(8, 'interstellar_poster.jpg', ''),
(9, 'fight_club_poster.jpg', ''),
(10, 'warcraft_le_commencement_poster.jpg', ''),
(11, 'ready_player_one_poster.jpg', ''),
(12, 'infinity_war_poster.jpg', '');

-- --------------------------------------------------------

--
-- Structure de la table `movies`
--

DROP TABLE IF EXISTS `movies`;
CREATE TABLE IF NOT EXISTS `movies` (
  `id_movie` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `release_date` date NOT NULL,
  `director` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_movie`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `movies`
--

INSERT INTO `movies` (`id_movie`, `title`, `description`, `release_date`, `director`, `thumbnail`) VALUES
(1, 'Reservoir Dogs', 'Après un hold-up manqué des cambrioleurs de haut vol font leurs comptes dans une confrontation violente pour découvrir lequel d\'entre eux les a trahis.', '1992-09-02', 'Quentin Tarantino', 'reservoir_dogs.jpg'),
(2, 'Spider-Man', 'Après avoir été mordu par une araignée génétiquement modifiée qui s\'était échappée d\'un laboratoire il se découvre des pouvoirs surhumain', '2002-06-12', 'Sam Raimi', 'spiderman.jpg'),
(3, 'Scott Pilgrim', 'Scott Pilgrim a 22 ans vit à Toronto et joue dans le groupe de rock amateur Mais Scott ne pourra conquérir le cœur de Ramona qu\'après avoir vaincu ses sept ex maléfiques', '2010-12-01', 'Edgar Wright', 'scott_pilgrim.jpg'),
(4, 'Alice aux pays des merveilles', 'Alice désormais âgée de 19 ans retourne dans le monde fantastique qu\'elle a découvert quand elle était enfant. Alice s\'embarque alors dans une aventure extraordinaire où elle accomplira son destin.', '2010-03-24', 'Tim Burton', 'alice_in_wonderlands.jpg'),
(5, 'Coco', 'Depuis déjà plusieurs générations la musique est bannie dans la famille de Miguel. Un vrai déchirement pour le jeune garçon dont le rêve ultime est de devenir un musicien aussi accompli que son idole: Ernesto de la Cruz', '2017-11-29', 'Lee Unrich Adrian Molina', 'coco.jpg'),
(6, 'Les mondes de Ralph', 'Dans une salle d’arcade Ralph la casse est le héros mal aimé d’un jeu des années 80. Son rôle est simple : il casse tout ! Pourtant il ne rêve que d’une chose être aimé de tous…', '2012-12-05', 'Rich Moore', 'les_mondes_de_ralph.jpg'),
(7, 'La cité de la peur', 'Une comédie familiale', '1994-03-09', 'Alain Berberian', 'lcdlp_photo1.jpg'),
(8, 'Interstellar', 'La recherche d\'une destination pour la migration de l\'espèce humaine', '2014-11-05', 'Christopher Nolan', 'interstellar.jpg'),
(9, 'Fight Club', 'La rencontre entre une vie banale et l\'anti-conformisme', '1999-11-10', 'David Fincher', 'fight_club.jpg'),
(10, 'Warcraft, le commencent', 'Adaptation du premier opus des jeux de la série Warcraft, les Orcs envahissent le monde d\'Azeroth', '2016-05-24', 'Duncan Jones', 'warcraft.jpg'),
(11, 'Ready player One', 'Le monde de 2045 est nul,  tous le monde préfère vivre dans son monde virtuel', '2018-03-28', 'Steven Spielberg', 'ready_player_one.jpg'),
(12, 'Avengers: Infinity war', 'Thanos menace l\'équilibre de l\'univers tout entier, les héros doivent lutter pour empêcher la purge inter-planétaire', '2018-04-23', 'Anthony et Joe Russo', 'infinity_war.jpg');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `have`
--
ALTER TABLE `have`
  ADD CONSTRAINT `have_genres_FK` FOREIGN KEY (`id_genre`) REFERENCES `genres` (`id_genre`),
  ADD CONSTRAINT `have_movies0_FK` FOREIGN KEY (`id_movie`) REFERENCES `movies` (`id_movie`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
