<?php
// include mysql.php file for $user and $password variables
include '../mysql.php';

function database()
{
    global $user;
    global $password;

    try
    {
        $db = new PDO('mysql:host=localhost;dbname=bdd_annuaire;charset=utf8',$user,$password);
    }
    catch(Exception $e)
    {
        die('Erreur : '. $e->getMessage());
    }
    return $db;
}

function movie($id) {
    $pdo = database();

    $value = array();

    $movie = $pdo -> prepare('SELECT *
        FROM movies
        WHERE movies.id_movie = :id');

    $movie -> execute(['id' => $id]);

    while ($row = $movie -> fetch()){
        array_push($value, $row);
    }
    return $value;
}

function genre($id) {
    $pdo = database();

    $value = array();

    $movie = $pdo -> prepare('SELECT name
        FROM genres
        INNER JOIN have
        ON have.id_genre = genres.id_genre
        INNER JOIN movies
        ON have.id_movie = movies.id_movie
        WHERE movies.id_movie = :id');

    $movie -> execute(['id' => $id]);

    while ($row = $movie -> fetch()){
        array_push($value, $row);
    }
    return $value;
}

function poster($id) {
    $pdo = database();

    $value = array();

    $movie = $pdo -> prepare('SELECT image
        FROM images
        INNER JOIN have_img
        ON have_img.id_img = images.id_img
        INNER JOIN movies
        ON have_img.id_movie = movies.id_movie
        WHERE movies.id_movie = :id');

    $movie -> execute(['id' => $id]);

    while ($row = $movie -> fetch()){
        array_push($value, $row);
    }
    return $value;
}

function actor($id) {
    $pdo = database();

    $value = array();

    $movie = $pdo -> prepare('SELECT actor
        FROM actor
        INNER JOIN have_actor
        ON have_actor.id_actor = actor.id_actor
        INNER JOIN movies
        ON have_actor.id_movie = movies.id_movie
        WHERE movies.id_movie = :id');

    $movie -> execute(['id' => $id]);

    while ($row = $movie -> fetch()){
        array_push($value, $row);
    }
    return $value;
}
